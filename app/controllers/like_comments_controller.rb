class LikeCommentsController < ApplicationController
    before_action :find_comment
    def create
      if !already_liked_comment?
        @comment.like_comments.create(user_id: current_user.id)
      end
      redirect_to post_path(@comment.post)
    end
    
    def find_comment
      @comment = Comment.find(params[:comment_id])
    end
    private
    def already_liked_comment?
        LikeComment.where(user_id: current_user.id, comment_id:
        params[:comment_id]).exists?
        
    end
end
