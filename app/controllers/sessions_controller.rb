class SessionsController < ApplicationController
  before_action :chega_de_logar, only: [:new, :create]

  
  def new
  end
  
  def create
    user = User.find_by(username:params[:session][:username].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to root_path
    else
      flash.now[:danger] = 'Combinação de username e senha inválida.'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to users_path
  end
  
end
