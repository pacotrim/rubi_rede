class FeedController < ApplicationController
    before_action :redireciona
    def index
        @friendships=Friendship.all
        @posts=Post.all
        amigos=[]
        @friendships.each do |friend|
            if (friend.user_id==current_user.id)
                amigos.push(friend.friend_id)
            elsif (friend.friend_id==current_user.id)
                amigos.push(friend.user_id)
            end
        end
        @postagens=[]
        @posts.each do |p|
            if amigos.include? p.user_id
                @postagens.push(p)
            end
        end
        @postagens.sort_by(&:created_at)
    end
end