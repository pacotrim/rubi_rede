module SessionsHelper
    # Logs in the given user.
    def log_in(user)
        session[:user_id] = user.id
    end
    
    # Returns the current logged-in user (if any).
    def current_user
        @current_user ||= User.find_by(id: session[:user_id])
    end
    
    # Returns true if the user is logged in, false otherwise.
    def logged_in?
        !current_user.nil?
    end

    # If the user is not logged in, redirects to the login page.
    def redireciona
        if !logged_in?
            redirect_to login_path
        end
    end

    # Logs out the current user.
    def log_out
        session.delete(:user_id)
        @current_user = nil
    end

    #If the user is already logged in, redirects out of the login page
    def chega_de_logar
        if logged_in?
            redirect_to root_path
        end
    end
end
