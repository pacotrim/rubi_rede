class User < ApplicationRecord
    has_many :posts
    has_many :comments
    has_many :likes
    has_many :like_comments
    has_secure_password
    validates :password, presence: true, length: { minimum: 3 }
    has_many :friendships
    has_many :friends, :through => :friendships
    has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
    has_many :inverse_friends, :through => :inverse_friendships, :source => :user


    def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(string,cost: cost)
    end
    
end