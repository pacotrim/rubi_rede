Rails.application.routes.draw do
  root to: 'feed#index'
  resources :posts do
    resources :comments
  end
  resources :posts do
    resources :likes
  end
  resources :comments do
    resources :like_comments
  end
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users
  resources :friendships
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
